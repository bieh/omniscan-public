################################################################################
# Super simple executing agent
################################################################################

ENDPOINT = "https://app.omniscan.io" 
#ENDPOINT = "http://10.211.55.2:5000"

import subprocess
import requests
import time
import uuid
import os
import urllib
import urllib.request

def get_machine_id():
	macaddr = uuid.getnode()
	macaddr = ':'.join(("%012X" % macaddr)[i : i + 2] for i in range(0, 12, 2))
	return macaddr

def main():
	print("Starting up")

	# Report we're live
	register_url = ENDPOINT + "/api/v1/remote/online?platform=windows&username=none&auth=none&port=0&host=" + get_machine_id()

	# Start dispatching commands
	while True:
		requests.get(register_url)

		r = requests.get(ENDPOINT + "/api/v1/remote/dispatcher?host=" + get_machine_id())

		message = r.json()

		print(message)

		if "result" not in message:
			time.sleep(1)
			continue

		ret = []

		for command in message["result"]["commands"]:

			if isinstance(command, dict):
				if command["type"] == "download":
					opener = urllib.request.build_opener()
					opener.addheaders = [('User-agent', 'Mozilla/5.0')]
					urllib.request.install_opener(opener)
					urllib.request.urlretrieve(command["url"], command["dst"])
					cmdout = "ok"
			else:
				cmdout = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].decode("utf-8")
				

			ret.append(cmdout)

		print(ret)

		requests.post(ENDPOINT + "/api/v1/remote/response?guid=" + message["result"]["guid"] + "&host=" + get_machine_id(), json=[cmdout])

		print("Handled command")

main()